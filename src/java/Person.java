import framework.ModelView;
import framework.UrlMapping;
import java.sql.Date;
import java.util.HashMap;

/**
 *
 * @author tahina
 */
public class Person {
    
    String name;
    double money; 
    Date birthday;

    public Person(String name, double money, Date birthday) {
        this.name = name;
        this.money = money;
        this.birthday = birthday;
    }

    public Person() {
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
    
    
    
    public void sayTay(){
        System.out.println("Tay ^_^");
    }
    
    @UrlMapping(url = "hello")
    public ModelView sayHello(){
        System.out.println("Hello ^_^");
        String url = "Hello.jsp";
        ModelView mv = new ModelView();
        mv.setUrl(url);
        HashMap<Integer, String> h = new HashMap();      
        h.put(1, "Bonjour");
        h.put(2, "Ohayo");
        mv.setData(h);
        return mv;
        
    }
    
    @UrlMapping(url = "doubleMoney")
    public ModelView doubleMoney(){
        this.money = money*2;
        ModelView mv = new ModelView();
        mv.setUrl("doubled.jsp");
        HashMap<Double, Double> h = new HashMap();
        h.put(this.money, money*2);
        mv.setData(h);
        return mv;        
    }
    
    
    
}
