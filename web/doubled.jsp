<%-- 
    Document   : doubled
    Created on : Nov 18, 2022, 10:32:03 PM
    Author     : tahina
--%>

<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <p>Somme double</p>
        <%
            HashMap<Double, Double> h = (HashMap)request.getAttribute("data");
            out.println(h.toString());
        %>
    </body>
</html>
